package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	ArrayList<FridgeItem> fridgeItems;
	private final int maxsize= 20;
	
	Fridge (){
		fridgeItems = new ArrayList<>();
		
	}

	@Override
	public int nItemsInFridge() {
		return fridgeItems.size();
		
	}

	@Override
	public int totalSize() {
		return maxsize;
		
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge()<totalSize()) {
			fridgeItems.add(item);
			return true;
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if(fridgeItems.isEmpty()) {
			throw new NoSuchElementException();
		}
		else {
			fridgeItems.remove(item);
			
		}
		
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
	
			fridgeItems.clear();
		}	

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem>expiredItems = new ArrayList<>();
		for (FridgeItem item: fridgeItems ) {
			if(item.hasExpired()) {
				expiredItems.add(item);			
		    }	
			
		}
		for(FridgeItem item : expiredItems) {
			fridgeItems.remove(item);
		}
		return expiredItems;
	}

}
